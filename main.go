package main

import (
	"fmt"
)

const (
	robotName = "Aid"
)

func main() {
	greet("2020")
	getName()
	getAge()
	count()
	startQuiz()
	fmt.Println("Congratulations, have a nice day!")
}

func greet(year string) {
	fmt.Println("Hello! My name is " + robotName + ".")
	fmt.Println("I was created in " + year + ".")
}

func getName() {
	var userName string

	fmt.Println("Please, remind me your name.")
	fmt.Scan(&userName)
	fmt.Println("What a great name you have, " + userName + "!")
}

func getAge() {
	var remainder3 int
	var remainder5 int
	var remainder7 int

	fmt.Println("Let me guess your age.")
	fmt.Println("Enter remainders of dividing your age by 3, 5 and 7.")
	fmt.Scan(&remainder3)
	fmt.Scan(&remainder5)
	fmt.Scan(&remainder7)
	userAge := (remainder3*70 + remainder5*21 + remainder7*15) % 105
	fmt.Println("Your age is", userAge, ": that's a good time to start programming!", userAge)
}

func count() {
	var number int

	fmt.Println("Now I will prove to you that I can count to any number you want.")
	fmt.Scanf("%d", &number)

	for i := 0; i <= number; i++ {
		fmt.Println(i, "!")
	}
}

func startQuiz() {
	fmt.Println("Let's test your programming knowledge.")
	fmt.Println("Why do we use methods?")
	fmt.Println("1. To repeat a statement multiple times.")
	fmt.Println("2. To decompose a program into several small subroutines.")
	fmt.Println("3. To determine the execution time of a program.")
	fmt.Println("4. To interrupt the execution of a program.")
	getAnswer()
	fmt.Println("Completed, have a nice day!")
}

func getAnswer() {
	var answer int

	for answer != 2 {
		fmt.Scan(&answer)
		if answer != 2 {
			fmt.Println("Please, try again.")
		}
	}
}
